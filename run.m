
% Passed using arguments : 
% matlab -nodesktop -nodisplay -nosplash -r "path_ref='../../data/Scenes/Appart1opt02/appartAopt_00900.png';path_noisy='../../data/Scenes/Appart1opt02/appartAopt_00050.png';prefix='vdp_appart';run;exit"

%path_ref = '../../data/Scenes/Appart1opt02/appartAopt_00900.png'
%path_noisy = '../../data/Scenes/Appart1opt02/appartAopt_00050.png'
%prefix = 'passed as argument'

T = imread(path_noisy); % images must be normalized 0-1
R = imread(path_ref);

% normalized
T = double(T) / 255.
R = double(R) / 255.


% Compute pixels per degree for the viewing conditions
ppd = hdrvdp_pix_per_deg( 21, [size(T,2) size(T,1)], 1 );

% Run hdrvdp
res1 = hdrvdp( T, R, 'sRGB-Display', 30)

%vdp_img = imshow( hdrvdp_visualize( 'diff', res1.P_map, T, R, {'context_image', T} ) )

% output of 
c_map_computed = res1.C_map

c_map_computed(c_map_computed < 0) = 10e-5
c_map_norm = c_map_computed / (max(max(c_map_computed)))

img =  hdrvdp_visualize(c_map_computed);
img_mask =  hdrvdp_visualize(c_map_computed, T);
img_norm =  hdrvdp_visualize(c_map_norm);
img_norm_mask =  hdrvdp_visualize(c_map_norm, T);

disp(prefix)

imwrite(img, strcat(prefix, '.png'));
%imwrite(img_mask, strcat(prefix, '_mask.png'));
%imwrite(img_norm, strcat(prefix, '_norm.png'));
%imwrite(img_norm_mask, strcat(prefix, '_norm_mask.png'));

fileID = fopen(strcat(prefix, '_q_max.txt'),'w');
fprintf(fileID, '%3.5f', res1.Q);
fclose(fileID);